The different events.js.* files contain different animation sequences

To view the sequence rename it to events.js and load the file main.x3d in InstantPlayer downloadable at http://www.instantreality.org/downloads/

The different scenes are

events.js.simpleexample -> the example listing in the paper
events.js.morphing -> a morping example
events.js.framework -> an animation showing the process of event handling in the framework
events.js.nikolausdeko -> an example animation shown an eulerian tour with decorative animations
events.js.nikolausrep -> an example animation shown an eulerian tour with an representation object
events.js.nikolauscombined -> animation combining decorative animations and animation via representation object
events.js.representational -> animation showing decorative animations, representative animation, morphing of structures and the additional view elements controllable with the framework
