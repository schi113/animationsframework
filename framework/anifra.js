// The AniFrame-Object itself encapsulates only a very small functionality but provides the possibility to extend it
// functions of the framework itself: 
// the creation of a grouping node to hold the elements to display
// the creation of functions for the script node
// the creation of all necessary timesensor nodes and routes to the script and the functions therein
// the encapsulation of modules, that are able to handle different aspects of the scene

var AniFrame = (function() {
	 var modules = ['handler/node_event_handler.js', 
						 'handler/edge_event_handler.js', 
						 'handler/hud_event_handler.js', 
						 'handler/onion_layout_handler.js', 
						 'handler/move_node_handler.js', 
						 'handler/node_property_change_handler.js',
						 'handler/player_interface_event_handler.js',
						 'handler/clock_node_combining_handler.js',
						 'handler/create_move_delete_combining_handler.js',
						 'handler/event_stream_provider.js',
						];
	 var framework_script_node = Browser.currentScene.getNamedNode('AniFra');
	 var only_one_event_per_timestamp = false;
	 var max_events_per_timestamp = 500;
	 // this will be the returned object
	 var framework = {};
	 var start_time = undefined;
	 
	 var scene_timer_id = 0;
	 var xhr = new XMLHttpRequest();

	 var uid_counter = 0;
	 // the global object to inject functions into it
	 var global = (function(){return this;}).call();
	 //this local variable holds the different modules
	 var event_provider = {};

	 frameworkSceneTime = 0;
	 var last_timestamp = 0;

	 
	 // this variable will hold the events
	 var events = [];

	 // this variable will hold the handled events
	 var handled_events = [];

	 var execution_time_multiplicator = 1;

	 // this property will hold the actions that can be handled by the framework, 
	 // it property names will hold the name of the action and the respective values will hold 
	 // the references to the handler_modules capable of handling the action
	 var actions = {};

	 // the main node to insert elements into
	 var main_node;
	 
	 // the main script
	 var main_script;

	 // the groups for holding the nodes
	 var dynamic_group_nodes;
	 var dynamic_group_edges;
	 

	 

	 function tickFunction(timestamp) {
		  newEventsToHandleByHandlers = new MFString();
		  if (start_time !== undefined) {
				frameworkSceneTime += (timestamp - last_timestamp) * 1000 * execution_time_multiplicator;
				last_timestamp = timestamp;
				if (only_one_event_per_timestamp) {
					 handleNextEventFromEventStream(frameworkSceneTime);
				} else {
					 handleNextEventsFromEventStream(frameworkSceneTime);
				}
		  } else {
				start_time = timestamp;
				last_timestamp = timestamp;
				
		  }
	 }


	 function handleNextEventsFromEventStream(actual_time) {
		  var count = 0;
		  while (events[0] && events[0].time < actual_time) {
				// shift removes and returns the first element
				count++;
				if (count > max_events_per_timestamp) {
					 return;
				}
				handleEvent(events.shift());
				
		  }
		  
		  
		  
	 }

	 function handleNextEventFromEventStream(actual_time) {
		  if (events[0] && events[0].time < actual_time) {
				// shift removes and returns the first element
				handleEvent(events.shift());
		  }
	 }

	 function handleEvent(event) {
		  event['time'] = framework.getActualTime();
		  //handled_events.push(event);
		  
		  newEventsToHandleByHandlers[newEventsToHandleByHandlers.length] = JSON.stringify(event);
	 }

	 function findNodeByTypeAndDef(type, name) {
		  possible_nodes = Browser.currentScene.refNode.collectNodesByTypes([new SFString(type)]);
		  for (var i=0; i < possible_nodes.length; i++)  {
				if (possible_nodes[i].getNodeName() === name) {
					 return possible_nodes[i];
				}
		  }
		  return undefined;
	 }


	 function prepareScene() {
		  main_node = findNodeByTypeAndDef('Group', 'MAINGROUP');
		  if (main_node === undefined) {
				Browser.println("Create main grouping node");
				main_node = Browser.currentScene.createNode('Group');
				Browser.currentScene.refNode.addChild(main_node);
		  }
		  dynamic_group_nodes = findNodeByTypeAndDef('Group', 'NODEGROUP');
		  if (dynamic_group_nodes === undefined) {
				Browser.println("Create node grouping node");
				dynamic_group_nodes = Browser.currentScene.createNode('Group');
				main_node.addChild(dynamic_group_nodes);
		  }
		  dynamic_group_edges = findNodeByTypeAndDef('Group', 'EDGEGROUP');
		  if (dynamic_group_edges === undefined) {
				Browser.println("Create edge grouping node");
				dynamic_group_edges = Browser.currentScene.createNode('Group');
				main_node.addChild(dynamic_group_edges);
		  }
	 }


 
	 // the initialization of the framework
	 framework.initialize = function(){
		  // add the necessary nodes in the scene
		  prepareScene();
		  // add the necessary timesensors and routes to the scene
		  //prepareTimeSensorsAndRoutes();

		  // set the tick-Function
		  global.prepareEvents = tickFunction;

		  global.setSpeed = function(value, timestamp) {
				Browser.println('Speed: ' + value);
				framework.setSceneSpeed(value);
		  }


		  global.handleEvents = function(events_array, timestamp) {
				for (var i = 0; i < events_array.length; i++) {
					 // Browser.println('Handle Event: ' + events_array[i]);
					 var event = JSON.parse(events_array[i]);
					 if (event != undefined) {
						  if (event['time'] == 0) {
								framework.handleImmediateEvent(event);
						  } else {
								framework.insertEvent(event);
						  }
					 } else {
						  Browser.println("####################");
						  Browser.println(event);
					 }
				}
		  }

		  // set the initialize-Function
		  global.initialize = function() {
				var node_group = Browser.currentScene.getNamedNode('NODEGROUP');
				var edge_group = Browser.currentScene.getNamedNode('EDGEGROUP');
				var script = Browser.currentScene.getNamedNode('AniFra');
				for (var i = 0; i < modules.length; i++) {
					 var module = new SFNode('HandlerModule');
					 module.handlerCode[0] = modules[i];
					 Browser.currentScene.refNode.addChild(module);
					 Browser.currentScene.addRoute(node_group, 'children', module, 'nodeGroupChildren');
					 Browser.currentScene.addRoute(edge_group, 'children', module, 'edgeGroupChildren');
					 Browser.currentScene.addRoute(script, 'newEventsToHandleByHandlers', module, 'eventsToHandle');
					 Browser.currentScene.addRoute(module, 'newEvents', script, 'handleEvents');
					 Browser.currentScene.addRoute(script, 'frameworkSceneTime', module, 'frameworkSceneTime');
				}
				node_group.children = node_group.children;
				edge_group.children = edge_group.children;
		  }
	 }


	 framework.insertEvent = function(event){
		  Browser.println("Insert event " + event.action + " time " + event.time);
		  if (event.time !== undefined) {
				// if (event.time == 0) {
				//  	 handleEvent(event);
				//  	 return;
				// }
				
				if (events.length > 0) {
					 if (event.time === 0) {
						  //Browser.println('Insert Event at front behind other immediate ' + events.length);
						  for (var i = 0; i < events.length; i++) {
								if (events[i].time > 0) {
									 events.splice(i, 0, event);
									 Browser.println(events);
					 				 return;
					 			}
						  }
					 	  events.push(event);
						  
						  return;
					 }
					 // event-time is lower than all other times
					 if (event.time < events[0].time) {
					 	  //Browser.println('Insert Event at front');
					 	  //Browser.println(events.length);
					 	  events = [event].concat(events);
					 	  return 
					 }
					 // event-time is higher than all other times
					 if (event.time >= events[events.length - 1].time) {
						  //Browser.println('Insert Event at back');
						 // Browser.println(events.length);
						  events.push(event);
						  return 
					 }
					 // search for the element to insert after
					 for (var i = 0; i < events.length - 1; i++) {
						  if (event.time >= events[i].time && event.time <= events[i + 1].time) {
								//Browser.println('Insert Event at ' + (i + 1));
								//Browser.println(events.length);
								events.splice(i + 1, 0, event);
								return
						  } 
					 }
					 Browser.println("Ooh");
				} else {
					 // there were no elements in the array
					 //Browser.println('Insert first Event');
					 //Browser.println(events.length);
					 events[0] = event;
					 return 
				}
		  } else {
				Browser.println('Event hat nicht das richtige Format');
		  }
		  
	 }

	 // bulk insertion of events, the events will be sorted after insertion
	 framework.insertEvents = function(tmp_events) {
		  //Browser.println("Importiere " + tmp_events.length + " Events");
		  for (var i = 0; i < tmp_events.length; i++) {
				//Browser.print("Call insertEvent");
				framework.insertEvent(tmp_events[i]);
		  }
	 }
	 
	 // add an action to the list of possible actions and connect it to a handler capable of handling the action
	 framework.addAction = function(action, handler) {
		  actions[action] = handler;
	 }

	 // add a list of actions to the list of possible actions and connect it to a handler capable of handling the action
	 // action_hash: Object with names of the actions as properties
	 framework.addActions = function(action_hash, handler) {
		  for (var action in action_hash) {
		   	framework.addAction(action, handler);
		  }
	 }


	 // remove an action to the list of possible actions 
	 framework.removeAction = function(action) {
		  actions[action] = undefined;
	 }

	 // immediately call the handler for an event, this function can be used to generate interaction events
	 framework.handleImmediateEvent = function(event) {
		  // setTimeout(function() {
					 handleEvent(event);
		  // }, 1);
		  
	 }


	 // add an event_provider to the list of event providers
	 framework.addEventProvider = function(name, provider) {
		  event_provider[name] = provider;
		  
	 }

	 // remove an event_provider to the list of event providers
	 framework.removeEventProvider = function(name) {
		  event_provider[name] = undefined;
	 }

	 // retrieve an event_provider from the list of event providers
	 framework.getEventProvider = function(name) {
		  return event_provider[name];
	 }

	 // get the group of nodes dynamically added to the scene
	 framework.nodes = function() {
		  return dynamic_group_nodes;
	 }

	 // get the group of edges dynamically added to the scene
	 framework.edges = function() {
		  return dynamic_group_edges;
	 }

	 framework.mainScriptNode = function() {
		  return framework_script_node;
	 }
	 


	 // get a node by its name
	 framework.findNodeByName = function(name) {
		  for (var a = 0; a < dynamic_group_nodes.children.length; a++) {
				if(dynamic_group_nodes.children[a].name == name) {
					 return dynamic_group_nodes.children[a];
				}
		  }
	 }


	 // get a node by its id
	 framework.findNodeByID = function(id) {
		  for (var a = 0; a < dynamic_group_nodes.children.length; a++) {
				if(dynamic_group_nodes.children[a].id == id) {
					 return dynamic_group_nodes.children[a];
				}
		  }
	 }

	 // get an edge by its name
	 framework.findEdgeByName = function(name) {
		  for (var a = 0; a < dynamic_group_edges.children.length; a++) {
				if(dynamic_group_edges.children[a].name == name) {
					 return dynamic_group_edges.children[a];
				}
		  }
	 }



	 // get an edge by its id
	 framework.findEdgeByID = function(id) {
		  for (var a = 0; a < dynamic_group_edges.children.length; a++) {
				if(dynamic_group_edges.children[a].id == id) {
					 return dynamic_group_edges.children[a];
				}
		  }
	 }

	 // get the actual elapsed time
	 framework.getActualTime = function() {
		  return frameworkSceneTime;
	 }

	 // function to complete the received parameters for an event with the default parameters  
	 framework.getCompletedParameters = function(default_parameters, actual_parameters) {
		  for(var param in default_parameters){
				if(actual_parameters[param] === undefined) {
					 actual_parameters[param] = default_parameters[param];
				}
		  }
		  return actual_parameters;
	 }


	 framework.registerNotification = function(action, parameters) {
		  framework.insertEvent({"time": 0, "action": action, "parameters" : parameters});
	 }

	 framework.getUID = function() {
		  return 'UID' + uid_counter++;
	 }


	 // does not work
	 // framework.addExternalProtoDeclare = function(name, url) {
	 // 	  var proto_declare = Browser.currentScene.createNode("ExternProtoDeclare");
	 // 	  proto_declare.name = name;
	 // 	  proto_declare.url = url;
	 // 	  Browser.currentScene.refNode.addChild();
	 // }

	 framework.setSceneSpeed = function(value) {
		  var timerjob = Browser.currentScene.getNamedNode('engine::defaultTimerJob');
		  timerjob.speed = value;
	 }

	 // test function
	 framework.test = function() {
		  //framework.insertEvents([{'action': 'addNode', 'time' : 1000},{'action': 'removeNode', 'time' : 2000}, {'action': 'removeNode', 'time' : 3000}, {'action': 'blubb', 'time' : 4000}]);
	 }

	 framework.initialize();
	 return framework;
})();


// importScripts('framework/modules/handler/edge_event_handler.js');
// importScripts('framework/modules/handler/node_event_handler.js');
// importScripts('framework/modules/handler/move_node_handler.js');
// importScripts('framework/modules/handler/onion_layout_handler.js');
// importScripts('framework/modules/handler/decoration_event_handler.js');
// importScripts('framework/modules/handler/representational_node_handler.js');
// importScripts('framework/modules/handler/dummy_event_handler.js');
// importScripts('framework/modules/handler/hud_event_handler.js');
// importScripts('framework/modules/handler/node_property_change_handler.js');
// importScripts('framework/modules/handler/clock_node_combining_handler.js');
// importScripts('framework/modules/handler/create_move_delete_combining_handler.js');
// importScripts('framework/modules/handler/player_interface_event_handler.js');
//importScripts('framework/modules/provider/event_stream_provider.js');
//EventStreamProvider.readEventStreamFile('events.js');

