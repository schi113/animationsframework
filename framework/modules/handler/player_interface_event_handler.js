var PlayerInterfaceEventHandler = (function(){
	 var handler = {};
	 var global = (function(){return this;}).call();
	 var main_script_node;
	 var ref_node;
	 var nodes_group;
	 var edges_group;
	 var actualFrameworkTime = 0;
	 var uid_counter = 0;


	 var default_parameters = {'value':1.0};


	 handler.getUID = function() {
		  return 'UIDNodeEventHandler' + uid_counter++;
	 }


	 // get a node by its name
	 handler.findNodeByName = function(name) {
		  for (var a = 0; a < nodes_group.children.length; a++) {
				if(nodes_group.children[a].name == name) {
					 return nodes_group.children[a];
				}
		  }
	 }


	 // get a node by its id
	 handler.findNodeByID = function(id) {
		  for (var a = 0; a < nodes_group.children.length; a++) {
				if(nodes_group.children[a].id == id) {
					 return nodes_group.children[a];
				}
		  }
	 }

	 // get an edge by its name
	 handler.findEdgeByName = function(name) {
		  for (var a = 0; a < edges_group.children.length; a++) {
				if(edges_group.children[a].name == name) {
					 return edges_group.children[a];
				}
		  }
	 }



	 // get an edge by its id
	 handler.findEdgeByID = function(id) {
		  for (var a = 0; a < edges_group.children.length; a++) {
				if(edges_group.children[a].id == id) {
					 return edges_group.children[a];
				}
		  }
	 }


	 handler.getCompletedParameters = function(default_parameters, actual_parameters) {
		  for(var param in default_parameters){
				if(actual_parameters[param] === undefined) {
					 actual_parameters[param] = default_parameters[param];
				}
		  }
		  return actual_parameters;
	 }


	 function addPlayerInterface(parameters) {
		  var node = new SFNode('PlayerInterface');
		  nodes_group.addChild(node);
		  Browser.currentScene.addRoute(node, 'speed', main_script_node, 'setSpeed');
		  Browser.currentScene.addRoute(main_script_node, 'frameworkSceneTime', node, 'time');
		  
	 }
	 


	 var actions = {
		  'addPlayerInterface': addPlayerInterface
	 };


	 handler.handleSpecificEvent = function(event) {
		  if (actions[event['action']] !== undefined) {
				actions[event['action']](event.parameters);
		  } // else {
		  // 		Browser.println("This Handler is not capable of handling the action " + event['action']);
		  // }
	 }
	 
	 handler.initialize = function(){
		  global.initialize = function() {
				Browser.println('Initialize HandlerModule');
		  }

		  global.nodeGroupChildren = function(value) {
		  		if (nodes_group === undefined) {
					 
					 
		  			 nodes_group = value[0].getParents()[0];
					 var search_node = nodes_group.getParents()[0];
					 while(search_node.getParents().length > 0) {
						  search_node = search_node.getParents()[0];
					 }
					 var script_nodes = search_node.collectNodesByTypes([new SFString('Script')]);
					 for (var i= 0; i < script_nodes.length; i++) {
		  				  if (script_nodes[i].getNodeName() == 'AniFra') {
		  						main_script_node = script_nodes[i];
		  						return;
		  				  }
		  			 }
		  		}
		  }

		  global.edgeGroupChildren = function(value) {
		  		if (edges_group === undefined) {
		  			 edges_group = value[0].getParents()[0];
		  		}
		  }


		  global.handleEvents = function(events_array) {
				for (var i = 0; i < events_array.length; i++) {
					 handler.handleSpecificEvent(JSON.parse(events_array[i]));
				}
		  }
		  global.frameworkSceneTime = function(value) {
				actualFrameworkTime = value;
		  }
	 }



	 handler.initialize();
	 return handler;
	 })();
