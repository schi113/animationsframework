function Handler(actions) {
	 var global = (function(){return this;}).call();
	 var handler = {};
	 var uidCounter = 0;
	 var nodesGroup;
	 var edgesGroup;
	 var actualFrameworkTime = 0;
	 var idName = Math.random().toString(36).substr(2,9)

	 handler.getNodesGroup = function() {
		  return nodesGroup;
	 }
	 
	 handler.getEdgesGroup = function() {
		  return edgesGroup;
	 }
	 
	 handler.getActualFrameworkTime = function() {
		  return actualFrameworkTime;
	 }
	 
	 handler.getUID = function() {
		  return 'UID' + idName + uidCounter++;
	 }


	 // get a node by its name
	 handler.findNodeByName = function(name) {
		  for (var a = 0; a < nodesGroup.children.length; a++) {
				if(nodesGroup.children[a].name == name) {
					 return nodesGroup.children[a];
				}
		  }
	 }


	 // get a node by its id
	 handler.findNodeByID = function(id) {
		  for (var a = 0; a < nodesGroup.children.length; a++) {
				if(nodesGroup.children[a].id == id) {
					 return nodesGroup.children[a];
				}
		  }
	 }

	 // get an edge by its name
	 handler.findEdgeByName = function(name) {
		  for (var a = 0; a < edgesGroup.children.length; a++) {
				if(edgesGroup.children[a].name == name) {
					 return edgesGroup.children[a];
				}
		  }
	 }



	 // get an edge by its id
	 handler.findEdgeByID = function(id) {
		  for (var a = 0; a < edgesGroup.children.length; a++) {
				if(edgesGroup.children[a].id == id) {
					 return edgesGroup.children[a];
				}
		  }
	 }


	 handler.getCompletedParameters = function(default_parameters, actual_parameters) {
		  for(var param in default_parameters){
				if(actual_parameters[param] === undefined) {
					 actual_parameters[param] = default_parameters[param];
				}
		  }
		  return actual_parameters;
	 }

	 handler.handleSpecificEvent = function(event) {
		  if (actions[event['action']] !== undefined) {
				actions[event['action']](event.parameters);
		  } // else {
		  // 		Browser.println("This Handler is not capable of handling the action " + event['action']);
		  // }
	 }
	 handler.initialize = function(){
		  global.initialize = function() {
				Browser.println('Initialize HandlerModule');
		  }

		  global.nodeGroupChildren = function(value) {
		  		if (nodesGroup === undefined) {
		  			 nodesGroup = value[0].getParents()[0];
		  		}
		  }

		  global.edgeGroupChildren = function(value) {
		  		if (edgesGroup === undefined) {
		  			 edgesGroup = value[0].getParents()[0];
		  		}
		  }

		  global.handleEvents = function(events_array) {
				for (var i = 0; i < events_array.length; i++) {
					 handler.handleSpecificEvent(JSON.parse(events_array[i]));
				}
		  }
		  global.frameworkSceneTime = function(value) {
				actualFrameworkTime = value;
		  }
	 }

	 return handler;
}
