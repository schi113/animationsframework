// !!!! EventStreamProvider
importScripts('handler/handler_prototype.js');

var EventStreamProvider = (function(){
	 var actions = {
		  'readEventStreamFile' : readEventStreamFile
 	 };
	 var global = (function(){return this;}).call();
	 var handler = new Handler(actions);
	 var default_parameters = {
		  'file_name': "../../events.js"
	 };

	 function readEventStreamFile(parameters) {
		  Browser.println('readEventStreamFile');
		  parameters = handler.getCompletedParameters(default_parameters, parameters);
		  newEvents = new MFString();
		  importScripts(parameters['file_name']);
		  for (var i = 0; i < events.length; i++) {
				Browser.println(JSON.stringify(events[i]));
				newEvents[newEvents.length] = JSON.stringify(events[i]);
		  }
	 }
	 handler.initialize();
	 global.initialize = function() {
		  
		  readEventStreamFile({});
		  
	 }
	 
	 return handler;
})();

