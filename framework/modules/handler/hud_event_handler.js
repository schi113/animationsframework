// !!!! HudEventHandler
importScripts('handler/handler_prototype.js');

var HudEventHandler = (function(){
	 var actions = {
		  'addTextHud': addTextHud, 
		  'changeTextHudText': changeTextHudText,
		  'removeTextHud': removeTextHud
	 };
	 var handler = new Handler(actions);

	 var default_parameters = {
		  'node_name': 'HUD',
		  'color': "1.0 1.0 1.0",
		  'text': 'Example Text',
		  'usedefaultlayout': false
	 };

	 function addTextHud(parameters) {
		  parameters = handler.getCompletedParameters(default_parameters, parameters);
		  var node = new SFNode('Hud');
		  if (typeof parameters.text == 'string') {
					 node.text = new MFString(parameters.text);
		  } else {
				var new_text = new MFString();
				for(var i = 0; i < parameters.text.length; i++) {
					 new_text[i] = parameters.text[i];
				}
				node.text = new_text;
		  }
		  node.id = parameters.node_name;
		  node.name = parameters.node_name;
		  node.color = new SFColor(parameters.color.split(" ")[0],parameters.color.split(" ")[1],parameters.color.split(" ")[2]);
		  node.usedefaultlayout = false;
		  handler.getNodesGroup().addChild(node);
	 }

	 function changeTextHudText(parameters) {
		  parameters = handler.getCompletedParameters(default_parameters, parameters);
		  var hud_node = handler.findNodeByName(parameters.node_name);
		  if (hud_node) {
				if (typeof parameters.text == 'string') {
					 hud_node.text = new MFString(parameters.text);
				} else {
					 var new_text = new MFString();
					 for(var i = 0; i < parameters.text.length; i++) {
						  new_text[i] = parameters.text[i];
					 }
					 hud_node.text = new_text;
				}
		  } else {
				Browser.println('HUD-Node not found');
		  }
	 }

	 function removeTextHud(parameters) {
		  parameters = handler.getCompletedParameters(default_parameters, parameters);
		  var hud_node = handler.findNodeByName(parameters.node_name);
		  if (hud_node) {
				handler.getNodesGroup().removeChild(hud_node);
		  } else {
				Browser.println('HUD-Node not found');
		  }
	 }


	 handler.initialize();
	 return handler;
	 })();
