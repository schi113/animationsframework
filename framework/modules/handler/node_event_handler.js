importScripts('handler/handler_prototype.js');

var NodeEventHandler = (function(){
	 var actions = {
		  'addNode': addNode, 
		  'removeNode': removeNode
	 };
	 var handler = new Handler(actions);


	 var node_initial_position = new SFVec3f(0,0,0);
	 var position_counter = 0;
	 var position_offset = 3;
	 var position_offset_increment = 3;


	 var default_parameters = {
		  'node_name': '', 
		  'node_type': 'CustomBox', 
		  'usedefaultlayout': true, 
		  'layout': undefined, 
		  'uselabel': true, 
		  'chaserduration': 1, 
		  'size': "2 2 2", 
		  'centerattraction': -1.0
	 };

	 
	 // the are certainly several more suitable positioning algorithms, 
	 // but this one allows to position the nodes in a predictable way and is relatively easy for preventing positioning on the same spots
	 function getInitialPosition() {
		  position_counter++;
		  if ((position_counter % 26) == 0) {
				position_counter = 0;
				return new SFVec3f(+position_offset, 0, 0);}
		  if ((position_counter % 25) == 0) {return new SFVec3f(-position_offset, 0, 0);}
		  if ((position_counter % 24) == 0) {return new SFVec3f(0, -position_offset, 0);}
		  if ((position_counter % 23) == 0) {return new SFVec3f(0, +position_offset, 0);}
		  if ((position_counter % 22) == 0) {return new SFVec3f(0, 0, -position_offset);}
		  if ((position_counter % 21) == 0) {return new SFVec3f(0, 0, +position_offset);}
		  if ((position_counter % 20) == 0) {return new SFVec3f(-position_offset, +position_offset, 0);}
		  if ((position_counter % 19) == 0) {return new SFVec3f(+position_offset, +position_offset, 0);}
		  if ((position_counter % 18) == 0) {return new SFVec3f(+position_offset, -position_offset, 0);}
		  if ((position_counter % 17) == 0) {return new SFVec3f(-position_offset, -position_offset, 0);}
		  if ((position_counter % 16) == 0) {return new SFVec3f(-position_offset,  0, +position_offset);}
		  if ((position_counter % 15) == 0) {return new SFVec3f(+position_offset,  0, +position_offset);}
		  if ((position_counter % 14) == 0) {return new SFVec3f(+position_offset,  0, -position_offset);}
		  if ((position_counter % 13) == 0) {return new SFVec3f(-position_offset,  0, -position_offset);}
		  if ((position_counter % 12) == 0) {return new SFVec3f(0, -position_offset, +position_offset);}
		  if ((position_counter % 11) == 0) {return new SFVec3f(0, +position_offset, +position_offset);}
		  if ((position_counter % 10) == 0) {return new SFVec3f(0, +position_offset, -position_offset);}
		  if ((position_counter %  9) == 0) {return new SFVec3f(0, -position_offset, -position_offset);}
		  if ((position_counter %  8) == 0) {return new SFVec3f(-position_offset, +position_offset, +position_offset);}
		  if ((position_counter %  7) == 0) {return new SFVec3f(-position_offset, -position_offset, +position_offset);}
		  if ((position_counter %  6) == 0) {return new SFVec3f(-position_offset, +position_offset, -position_offset);}
		  if ((position_counter %  5) == 0) {return new SFVec3f(+position_offset, -position_offset, +position_offset);}
		  if ((position_counter %  4) == 0) {return new SFVec3f(+position_offset, +position_offset, +position_offset);}
		  if ((position_counter %  3) == 0) {return new SFVec3f(+position_offset, +position_offset, -position_offset);}
		  if ((position_counter %  2) == 0) {return new SFVec3f(+position_offset, -position_offset, -position_offset);}
		  
		  position_offset += position_offset_increment;
		  return new SFVec3f(-position_offset, -position_offset, -position_offset);
		  
	 }




	 function addNode(parameters) {
		  //Browser.println("Add Node");
		  parameters = handler.getCompletedParameters(default_parameters, parameters);
		  var node = new SFNode(parameters['node_type']);
		  if (parameters['uid'] === undefined) {
				var uid = handler.getUID();
		  } else {
				var uid = parameters.uid;
		  }
		  Browser.println('Knoten hinzufuegen: ' + parameters.node_name);
		  if (parameters.color === undefined) {
				node.color = new SFColor(1, 1, 1);
		  } else {
				if (parameters.color.split(' ').length === 3) {
	 	  			 node.color = new SFColor(parameters.color.split(' ')[0], parameters.color.split(' ')[1], parameters.color.split(' ')[2]);
				} else {
					 Browser.println("Farbe nicht parsebar: " + parameters.color);
				}
		  }

		  if (parameters.emissivecolor === undefined) {
				node.emissivecolor = new SFColor(0, 0, 0);
		  } else {
				if (parameters.emissivecolor.split(' ').length === 3) {
	 	  			 node.emissivecolor = new SFColor(parameters.emissivecolor.split(' ')[0], parameters.emissivecolor.split(' ')[1], parameters.emissivecolor.split(' ')[2]);
				} else {
					 Browser.println("Farbe nicht parsebar: " + parameters.emissivecolor);
				}
		  }
		  

		  
		  
		  node.name = new SFString(parameters.node_name);
		  //Browser.println(node.name);
		  node.id = new SFString(uid);
		  if (parameters['uselabel'] === true) {
				if (parameters['label'] !== undefined ) {
					 node.label = new MFString(parameters['label']);
				} else {
					 node.label = new MFString(parameters.node_name);	 
				}
		  } else {
				node.label = new MFString('');
		  }
		  

		  if (parameters['initial_position'] === undefined) {
				node.initialposition = getInitialPosition();
				node.position = node.initialposition;
		  } else {
				if (parameters['initial_position'].split(' ').length === 3) {
					 var initial_position = new SFVec3f(parameters['initial_position'].split(' ')[0], parameters['initial_position'].split(' ')[1], parameters['initial_position'].split(' ')[2]);
					 node.position = initial_position;
					 node.initialposition =  initial_position;
				} else {
					 Browser.println("initial_position has wrong format");
					 return;
				}
		  }
		  
		  if (parameters.size.split(' ').length === 3) {
	 	  		node.size = new SFVec3f(parameters.size.split(' ')[0], parameters.size.split(' ')[1], parameters.size.split(' ')[2]);
		  } else {
				node.size = new SFVec3f(parameters.size, parameters.size, parameters.size);
		  }
		   
		  
		  
		  if (parameters['centerattraction'] !== undefined)  {
				node.centerattraction = new SFFloat(parameters['centerattraction']);
		  } else {
				node.centerattraction = new SFFloat(-1.0);
		  }
		  
		  node.layout = parameters['layout'];
		  node.usedefaultlayout = parameters['usedefaultlayout'];
		  node.chaserduration = parameters['chaserduration'];
		  node.centerattraction = parameters['centerattraction'];
		  handler.getNodesGroup().addChild(node);
	 }

	 function removeNode(parameters) {
		  Browser.println('Knoten entfernen: ' + parameters.node_name);
		  var node_to_remove = handler.findNodeByName(parameters.node_name);
		  if (node_to_remove) {
				handler.getNodesGroup().removeChild(node_to_remove);
		  } else {
				Browser.println('Knoten nicht gefunden: ' + parameters.node_name);
				newEvents = new MFString();
				newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"removeNode", "parameters":parameters});
		  }
	 }


	 

	 handler.initialize();
	 return handler;
})();
