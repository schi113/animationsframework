// !!!! ClockNodeCombiningEventHandler
importScripts('handler/handler_prototype.js');

var ClockNodeCombiningEventHandler = (function(){
	 var actions = {
		  'addClockAnimation': addClockAnimation, 
	 };

	 var handler = new Handler(actions);


	 var default_parameters = {
		  'position': "0.0 0.0 0.0", 
		  'size': "2.0 2.0 0.3", 
		  'clockface_color': '0.0 0.0 1.0', 
		  'hand_color': '1.0 1.0 0.0', 
		  'repeat': true, 
		  'duration': 4000,
		  'label' : 'Zeit'
	 };

	 
	 function addClockAnimation(parameters) {
		  newEvents = new MFString();
		  parameters = handler.getCompletedParameters(default_parameters, parameters);
		  var time = handler.getActualFrameworkTime();
		  //Browser.println(time);
		  var single_duration = parameters['duration']/12;
		  if (parameters['reusable_ids'] === undefined) {
				var size = new SFVec3f(parameters['size'].split(' ')[0],parameters['size'].split(' ')[1],parameters['size'].split(' ')[2]);
				var position = new SFVec3f(parameters['position'].split(' ')[0],parameters['position'].split(' ')[1],parameters['position'].split(' ')[2]);
				var position_face = position.x + " " + position.y + " " + (position.z - (2*size.z/3));
				var position_center = position.x + " " + position.y + " " + (position.z + (1*size.z/3));
				var size_face = size.x + " " + size.y + " " + (2*size.z/3);
				var radius = size.x / 2;
				var radius2 = radius * radius;
				var x_min_0 = position.x;
				var y_min_0 = position.y + radius;
				var term_1 = Math.sqrt(radius2)/2;
				var term_2 = (Math.sqrt(3) * Math.sqrt(radius2))/2;
				var pos_0 = x_min_0 + " " + y_min_0 + " " + (position.z + (1*size.z/3));
				var x_min_5 = position.x + term_1;
				var y_min_5 = position.y + term_2;
				var pos_5 = x_min_5 + " " + y_min_5 + " " + (position.z + (1*size.z/3));
				var x_min_10 = position.x + (term_2);
				var y_min_10 = position.y + term_1;
				var pos_10 = x_min_10 + " " + y_min_10 + " " + (position.z + (1*size.z/3));
				var x_min_15 = position.x + radius;
				var y_min_15 = position.y;
				var pos_15 = x_min_15 + " " + y_min_15 + " " + (position.z + (1*size.z/3));
				var x_min_20 = position.x + (term_2);
				var y_min_20 = position.y - term_1;
				var pos_20 = x_min_20 + " " + y_min_20 + " " + (position.z + (1*size.z/3));
				var x_min_25 = position.x + term_1;
				var y_min_25 = position.y - (term_2);
				var pos_25 = x_min_25 + " " + y_min_25 + " " + (position.z + (1*size.z/3));
				var x_min_30 = position.x;
				var y_min_30 = position.y - radius;
				var pos_30 = x_min_30 + " " + y_min_30 + " " + (position.z + (1*size.z/3));
				var x_min_35 = position.x - term_1;
				var y_min_35 = position.y - (term_2);
				var pos_35 = x_min_35 + " " + y_min_35 + " " + (position.z + (1*size.z/3));
				var x_min_40 = position.x - (term_2);
				var y_min_40 = position.y - term_1;
				var pos_40 = x_min_40 + " " + y_min_40 + " " + (position.z + (1*size.z/3));
				var x_min_45 = position.x - radius;
				var y_min_45 = position.y;
				var pos_45 = x_min_45 + " " + y_min_45 + " " + (position.z + (1*size.z/3));
				var x_min_50 = position.x - (term_2);
				var y_min_50 = position.y + term_1;
				var pos_50 = x_min_50 + " " + y_min_50 + " " + (position.z + (1*size.z/3));
				var x_min_55 = position.x - term_1;
				var y_min_55 = position.y + (term_2);
				var pos_55 = x_min_55 + " " + y_min_55 + " " + (position.z + (1*size.z/3));
				var pos_hrs = (x_min_10 - 0.6*(term_2)) + " " + (y_min_10 - 0.6*term_1) + " " + (position.z + (1*size.z/3));
				
				var node_id_clockface = handler.getUID();
				var node_id_center = handler.getUID();
				var node_id_u = handler.getUID();
				var node_id_uur = handler.getUID();
				var node_id_urr = handler.getUID();
				var node_id_r = handler.getUID();
				var node_id_drr = handler.getUID();
				var node_id_ddr = handler.getUID();
				var node_id_d = handler.getUID();
				var node_id_ddl = handler.getUID();
				var node_id_dll = handler.getUID();
				var node_id_l = handler.getUID();
				var node_id_ull = handler.getUID();
				var node_id_uul = handler.getUID();
				var node_id_mins = handler.getUID();
				var node_id_hrs = handler.getUID();
				var edge_id_min = handler.getUID();
				var edge_id_hr = handler.getUID();
				
				Browser.println('1');
				newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"addNode","parameters":{"node_name":node_id_clockface,"node_type":"CustomSphere","color":parameters['clockface_color'],"size":size_face,"initial_position": position_face, "use_label":true,"label":parameters['label']}});
		  		newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"addNode","parameters":{"node_name":node_id_center ,"node_type":"DummyNode","initial_position": position_center}});
		  		newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"addNode","parameters":{"node_name":node_id_mins, "node_type":"DummyNode","initial_position": pos_0}});
		  		newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"addNode","parameters":{"node_name":node_id_hrs, "node_type":"DummyNode","initial_position": pos_hrs}});
				newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"addNode","parameters":{"node_name":node_id_hrs, "node_type":"DummyNode","initial_position": pos_hrs}});
		  		newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"addNode","parameters":{"node_name":node_id_u ,"node_type":"DummyNode","initial_position": pos_0}});
		  		newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"addNode","parameters":{"node_name":node_id_uur ,"node_type":"DummyNode","initial_position": pos_5}});
				Browser.println('2');
		  		newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"addNode","parameters":{"node_name":node_id_urr ,"node_type":"DummyNode","initial_position": pos_10}});
		  		newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"addNode","parameters":{"node_name":node_id_r ,"node_type":"DummyNode","initial_position": pos_15}});
		  		newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"addNode","parameters":{"node_name":node_id_drr ,"node_type":"DummyNode","initial_position": pos_20}});
		  		newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"addNode","parameters":{"node_name":node_id_ddr ,"node_type":"DummyNode","initial_position": pos_25}});
		  		newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"addNode","parameters":{"node_name":node_id_d ,"node_type":"DummyNode","initial_position": pos_30}});
		  		newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"addNode","parameters":{"node_name":node_id_ddl ,"node_type":"DummyNode","initial_position": pos_35}});
		  		newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"addNode","parameters":{"node_name":node_id_dll ,"node_type":"DummyNode","initial_position": pos_40}});
		  		newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"addNode","parameters":{"node_name":node_id_l ,"node_type":"DummyNode","initial_position": pos_45}});
				Browser.println('3');
		  		newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"addNode","parameters":{"node_name":node_id_ull ,"node_type":"DummyNode","initial_position": pos_50}});
		  		newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"addNode","parameters":{"node_name":node_id_uul ,"node_type":"DummyNode","initial_position": pos_55}});										
				newEvents[newEvents.length] =	JSON.stringify( {"time":0,"action":"addEdge","parameters":{"edge_name":edge_id_min,"from":node_id_center, "to":node_id_mins,"color":parameters['hand_color']}});
		  		newEvents[newEvents.length] =	JSON.stringify( {"time":0,"action":"addEdge","parameters":{"edge_name":edge_id_hr,"from":node_id_center, "to":node_id_hrs,"color":parameters['hand_color']}});
				Browser.println('4');
				
		  } else {
				var node_id_clockface = parameters['reusable_ids']['node_id_clockface'];
				var node_id_center = parameters['reusable_ids']['node_id_center'];
				var node_id_u = parameters['reusable_ids']['node_id_u'];
				var node_id_uur = parameters['reusable_ids']['node_id_uur'];
				var node_id_urr = parameters['reusable_ids']['node_id_urr'];
				var node_id_r = parameters['reusable_ids']['node_id_r'];
				var node_id_drr = parameters['reusable_ids']['node_id_drr'];
				var node_id_ddr = parameters['reusable_ids']['node_id_ddr'];
				var node_id_d = parameters['reusable_ids']['node_id_d'];
				var node_id_ddl = parameters['reusable_ids']['node_id_ddl'];
				var node_id_dll = parameters['reusable_ids']['node_id_dll'];
				var node_id_l = parameters['reusable_ids']['node_id_l'];
				var node_id_ull = parameters['reusable_ids']['node_id_ull'];
				var node_id_uul = parameters['reusable_ids']['node_id_uul'];
				var node_id_mins = parameters['reusable_ids']['node_id_mins'];
				var node_id_hrs = parameters['reusable_ids']['node_id_hrs'];
				var edge_id_min = parameters['reusable_ids']['edge_id_min'];
				var edge_id_hr = parameters['reusable_ids']['edge_id_hr'];
				var events = [];
		  }
		  
		  
		  newEvents[newEvents.length] = JSON.stringify({"time":time,"action":"moveNode","parameters":{"node_name":node_id_mins,"target_node":node_id_uur, 'duration': single_duration}});
		  time += single_duration;
		  newEvents[newEvents.length] = JSON.stringify({"time":time,"action":"moveNode","parameters":{"node_name":node_id_mins,"target_node":node_id_urr, 'duration': single_duration}});
		  time += single_duration;
		  newEvents[newEvents.length] = JSON.stringify({"time":time,"action":"moveNode","parameters":{"node_name":node_id_mins,"target_node":node_id_r, 'duration': single_duration}});
		  time += single_duration;
		  newEvents[newEvents.length] = JSON.stringify({"time":time,"action":"moveNode","parameters":{"node_name":node_id_mins,"target_node":node_id_drr, 'duration': single_duration}});
		  time += single_duration;
		  newEvents[newEvents.length] = JSON.stringify({"time":time,"action":"moveNode","parameters":{"node_name":node_id_mins,"target_node":node_id_ddr, 'duration': single_duration}});
		  time += single_duration;
		  newEvents[newEvents.length] = JSON.stringify({"time":time,"action":"moveNode","parameters":{"node_name":node_id_mins,"target_node":node_id_d, 'duration': single_duration}});
		  time += single_duration;
		  newEvents[newEvents.length] = JSON.stringify({"time":time,"action":"moveNode","parameters":{"node_name":node_id_mins,"target_node":node_id_ddl, 'duration': single_duration}});
		  time += single_duration;
		  newEvents[newEvents.length] = JSON.stringify({"time":time,"action":"moveNode","parameters":{"node_name":node_id_mins,"target_node":node_id_dll, 'duration': single_duration}});
		  time += single_duration;
		  newEvents[newEvents.length] = JSON.stringify({"time":time,"action":"moveNode","parameters":{"node_name":node_id_mins,"target_node":node_id_l, 'duration': single_duration}});
		  time += single_duration;
		  newEvents[newEvents.length] = JSON.stringify({"time":time,"action":"moveNode","parameters":{"node_name":node_id_mins,"target_node":node_id_ull, 'duration': single_duration}});
		  time += single_duration;
		  newEvents[newEvents.length] = JSON.stringify({"time":time,"action":"moveNode","parameters":{"node_name":node_id_mins,"target_node":node_id_uul, 'duration': single_duration}});
		  time += single_duration;
		  newEvents[newEvents.length] = JSON.stringify({"time":time,"action":"moveNode","parameters":{"node_name":node_id_mins,"target_node":node_id_u, 'duration': single_duration}});
		  time += single_duration;

		  
		  
		  if (parameters['repeat']) {
				parameters['reusable_ids'] = {};
				parameters['reusable_ids']['node_id_clockface'] = node_id_clockface;
				parameters['reusable_ids']['node_id_center'] = node_id_center;
				parameters['reusable_ids']['node_id_u'] = node_id_u;
				parameters['reusable_ids']['node_id_uur'] = node_id_uur;
				parameters['reusable_ids']['node_id_urr'] = node_id_urr;
				parameters['reusable_ids']['node_id_r'] = node_id_r;
				parameters['reusable_ids']['node_id_drr'] = node_id_drr;
				parameters['reusable_ids']['node_id_ddr'] = node_id_ddr;
				parameters['reusable_ids']['node_id_d'] = node_id_d;
				parameters['reusable_ids']['node_id_ddl'] = node_id_ddl;
				parameters['reusable_ids']['node_id_dll'] = node_id_dll;
				parameters['reusable_ids']['node_id_l'] = node_id_l;
				parameters['reusable_ids']['node_id_ull'] = node_id_ull;
				parameters['reusable_ids']['node_id_uul'] = node_id_uul;
				parameters['reusable_ids']['node_id_mins'] = node_id_mins;
				parameters['reusable_ids']['node_id_hrs'] = node_id_hrs;
				parameters['reusable_ids']['edge_id_min'] = edge_id_min;
				parameters['reusable_ids']['edge_id_hr'] = edge_id_hr;
				newEvents[newEvents.length] = JSON.stringify({'time': time, 'action': 'addClockAnimation', 'parameters': parameters});
				
		  } else {
				
		  }
		  //Browser.println(handler.getActualFrameworkTime());

		  Browser.println('5');
	 }


	 handler.initialize();
	 return handler;
})();
