// !!!! ExampleEventHandler
var DecorationEventHandler = (function(anifra){
	 var handler = {};
	 var framework = anifra;
	 var nodes = framework.nodes();
	 var edges = framework.edges();
	 
	 var default_parameters = {
		  decoration_target: '', 
		  decoration_type: 'Pulsing', 
		  count: -1, 
		  factor: 2, 
		  func: 'sinus', 
		  one_run_duration: 2, 
		  sampling_rate_per_second: 10
	 };

	 var actions = {
		  'addDecorationToNode': decorate, 
	 };

	 function getNodeToDecorate(parameters) {
		  var node_to_decorate = framework.findNodeByName(parameters.decoration_target);
		  if (node_to_decorate === undefined) 
	  			node_to_decorate = framework.findEdgeByName(params.decoration_target);
		  if (node_to_decorate === undefined) {
				Browser.println("Knoten zum Dekorieren nicht gefunden");
		  }
		  return node_to_decorate;
	 }

	 function getDecorateHandle(node) {
		  transform_node = getNodeByTypeAndName(node, 'Transform', 'decoration_handle')
		  return transform_node;
	 }


	 function getFunctionValuesAsFloats(func_name, count, factor, one_run_duration, sampling_rate) {
		  // count = anzahl der durchläufe von 1 über factor hin zu 1
		  // factor = faktor des MAximalwertes
		  // one_run_duration: dauer für einen Durchlauf
		  // sampling_rate = wie oft wird die Funktion pro Sekunde abgetastet
		  var duration_for_complete_animation;
		  if (count === -1) {
				duration_for_complete_animation = one_run_duration;
		  } else {
				duration_for_complete_animation = count * one_run_duration * 1.0;
		  }
		  
		  var number_of_samples = duration_for_complete_animation * sampling_rate;
		  var value_array = [];
		  var key_array = [];
		  switch (func_name) {
		  case 'sinus':
				{
					 var pi = Math.PI;
					 for (var i = 0.0; i <= number_of_samples; i++) {
						  value_array.push((0.5 + (factor * 0.5)) - (0.5 * Math.cos((i/number_of_samples * duration_for_complete_animation/one_run_duration * pi * 2.0)) * (factor - 1.0)));
						  key_array.push(i/number_of_samples);
					 }
					 return {'value_array': value_array, 'key_array': key_array};
				}
		  }
	 }


	 function getNodeByTypeAndName(parent_node, type, name) {
		  var name_regex = new RegExp('^' + name);
		  if (parent_node) {
				var possible_nodes = parent_node.collectNodesByTypes(type);
				var possible_node;
				for(var i = 0; i < possible_nodes.length;i++) {
					 var node_name = possible_nodes[i].getNodeName();
					 
	 				 if (node_name && node_name.match(name_regex)) {
	 					  possible_node = possible_nodes[i];
	 				 }
				}
		  } else {
				Browser.println('Keinen Knoten angeben');
		  }
		  if (possible_node === undefined) {
				Browser.println('Keinen passenden Knoten zum Typ ' + type + ' mit dem Namen ' + name+ ' gefunden');
		  } 
		  return possible_node;
	 }


	 function removeOldPulsingDecoration(node) {
		  //pulsing_interpolator = this.getNodeByTypeAndName(node, 'PositionInterpolator', 'pulsing_interpolator');
		  //time_sensor = this.getNodeByTypeAndName(node, 'TimeSensor', 'pulsing_time_sensor');
		  //remove pulsing_time_sensor
		  //remove pulsing_position_interpolator
		  //remove pulsing_route_time_interpolator
		  //remove pulsing_route_interpolator_transform
	 }


	 function decorate(parameters) {
		  parameters = framework.getCompletedParameters(default_parameters, parameters);
		  if (parameters.count === 0) {
				// keine Wiederholung -> nichts tun
		  } else {
				var node_to_decorate = getNodeToDecorate(parameters);
				var transform_node = getDecorateHandle(node_to_decorate);
				Browser.println('Dekoriere');
				removeOldPulsingDecoration(transform_node);		  
				time_sensor = new SFNode('TimeSensor');
				position_interpolator = new SFNode('PositionInterpolator');
				if (parameters.count === -1) {
					 //unendliche Wiederholung
					 time_sensor.cycleInterval = parameters.one_run_duration;
					 time_sensor.loop = true;
					 var tmp_object = getFunctionValuesAsFloats(parameters.func, parameters.count, parameters.factor, parameters.one_run_duration, parameters.sampling_rate_per_second);
				} else {
					 time_sensor.cycleInterval = parameters.one_run_duration * parameters.count;
					 time_sensor.loop = false;
					 // wenn 2 mal für jeweils 1 sekunde die Animation ausgeführt wird benötigen wir einen cycleInterval von 2 Sekunden
					 
					 var tmp_object = getFunctionValuesAsFloats(parameters.func, parameters.count, parameters.factor, parameters.one_run_duration, parameters.sampling_rate_per_second);
				}
				var key = new MFFloat();
				var keyValue = new MFVec3f();
				for (var i = 0; i < tmp_object.key_array.length; i++) {
					 key[i] = tmp_object.key_array[i];
					 var this_key_value_float = tmp_object.value_array[i];
					 keyValue[i] = new SFVec3f(this_key_value_float, this_key_value_float, this_key_value_float);
				}
				position_interpolator.keyValue = keyValue;
				position_interpolator.key = key;
				
				transform_node.addChild(position_interpolator);
				transform_node.addChild(time_sensor);
				
				Browser.currentScene.addRoute(time_sensor, "fraction_changed", position_interpolator, 'set_fraction');
				Browser.currentScene.addRoute(position_interpolator, "value_changed", transform_node, 'set_scale');
				// Das das hier hinten steht ist kein Zufall sondern sorgt dafür, dass erst dann die Transformation gestartet wird, wenn auch alles da ist, was dafür gebraucht wird
				time_sensor.startTime = new Date().getTime()/1000;
		  }

	 }
	 function removePulsingDecoration(parameters) {
		  var node_to_decorate = getNodeToDecorate(parameters);
		  var transform_node = getDecorateHandle(node_to_decorate);
		  removeOldPulsingDecoration(transform_node);
	 }

	 

	 handler.handleSpecificEvent = function(event) {
		  if (actions[event['action']] !== undefined) {
				actions[event['action']](event.parameters);
		  } else {
				Browser.println("This Handler is not capable of handling the action " + event['action']);
		  }
	 }

	 framework.addActions(actions, handler);
	 return handler;
})(AniFrame);




