// 
var RepresentationalEventHandler = (function(anifra){
	 var handler = {};
	 var framework = anifra;
	 var nodes  = framework.nodes();
	 var edges = framework.edges();

	 
	 

	 var default_parameters = {
		  node_type: 'CustomSphere'
	 };

	 var decorational_node_default_parameters = {
		  node_type: 'CustomSphere', 		   
		  usedefaultlayout: false, 
		  layout: undefined, 
		  uselabel: false, 
		  size: "0.5,0.5,0.5"
	 }

	 
	 function createRepresentationalNode(uid, node_parameters){
		  Browser.println('create');
		  
		  var event = {time: 0, action: 'addNode', parameters: node_parameters};
		  framework.insertEvent(event);
		  
	 }


	 function moveRepresentationalNode(uid, move_parameters){
		  
		  
		  var event = {time: 0, action: 'addNode', parameters: node_parameters};
		  framework.insertEvent(event);
		  
	 }
	 

	 var actions = {
		  'representationalNodeAnimation': representationalNodeAnimation, 
	
	 };

	 function representationalNodeAnimation(parameters) {
		  var uid = framework.getUID();
		  var node_parameters;
		  if (parameters['node_parameters'] !== undefined) {
				node_parameters = framework.getCompletedParameters(decorational_node_default_parameters, parameters['node_parameters']);
		  } else {
				node_parameters = decorational_node_default_parameters;
		  }
		  createRepresentationalNode(uid, node_parameters);
		  
	 }

	 handler.handleSpecificEvent = function(event) {
		  if (actions[event['action']] !== undefined) {
				actions[event['action']](event.parameters);
		  } else {
				Browser.println("This Handler is not capable of handling the action " + event['action']);
		  }
	 }
	 framework.addActions(actions, handler);
	 return handler;
})(AniFrame);
