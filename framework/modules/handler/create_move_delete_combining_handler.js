// !!!! CreateMoveCombiningHandler
importScripts('handler/handler_prototype.js');

var CreateMoveCombiningHandler = (function(){
	 var actions = {
		  'createMoveDeleteCombined': createMoveDeleteCombined, 
	 };

	 var handler = new Handler(actions);

	 var default_parameters = {
		  'from_position': '0.0 0.0 0.0',
		  'to_position': '1.0 1.0 0.0',
		  'duration': 2000,
		  'node_name': '', 
		  'node_type': 'CustomSphere', 
		  'usedefaultlayout': false, 
		  'layout': undefined, 
		  'uselabel': false, 
		  'chaserduration': 1, 
		  'size': "2 2 2", 
		  'centerattraction': -1.0,
		  'repeat': true
	 };

	 

	 function createMoveDeleteCombined(parameters) {
		  newEvents = new MFString();
		  parameters = handler.getCompletedParameters(default_parameters, parameters);
		  var time = handler.getActualFrameworkTime() + 15;
		  var node_id = handler.getUID();
		  var events = [];
		  parameters['node_name'] = node_id;
		  parameters['initial_position'] = parameters['from_position'];
		  newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"addNode","parameters":parameters});
		  newEvents[newEvents.length] = JSON.stringify({"time":time,"action":"moveNode","parameters":{"node_name":node_id, "position": parameters['to_position'], 'duration': parameters['duration']}});
		  time += parameters['duration'];
		  newEvents[newEvents.length] = JSON.stringify({"time":time,"action":"removeNode","parameters":{"node_name":node_id}});
		  if (parameters['repeat']) {
				newEvents[newEvents.length] = JSON.stringify({"time":time,"action":"createMoveDeleteCombined","parameters":parameters});
		  }
	 }

	 handler.initialize();
	 return handler;
})();
