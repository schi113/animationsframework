importScripts('handler/handler_prototype.js');

var MoveNodeHandler = (function(){
	 var actions = {
		  'moveNode': moveNode, 
		  'moveNodeCircular': moveNodeCircular 
	 };	 
	 var handler = new Handler(actions);

	 var default_parameters = {position: "0.0 0.0 0.0", chaserduration: 0.001, duration: 10000, last_moving_duration : 0};

	 function setNewPosition(node, new_target_position) {
		  //Browser.println("Set Position: " + new_target_position);
		  node.position = new_target_position;
	 }

	 

	 function moveNode(parameters) {
		  newEvents = new MFString();
		  var node;
		  var actualtime = handler.getActualFrameworkTime();
		  parameters = handler.getCompletedParameters(default_parameters, parameters);
		  if (parameters['start_time'] === undefined) {
				parameters['start_time'] = actualtime;
		  }
		  if (parameters['end_time'] === undefined) {
				parameters['end_time'] = actualtime + parameters['duration'];
		  }
		  //Reason for doing this this way is the X3D-peculiarity to deliver the exact same time at the start and at te end of the function, so that the duration can not be measured by instrument the beginning and the end with a classic new Date().getTime()		  
		  if (parameters['last_start'] === undefined) {
				// set start value, will be refined in the next loops
				parameters['last_moving_duration'] = 15;
		  } else {
				parameters['last_moving_duration'] = actualtime - parameters['last_start'];
		  }
		  parameters['last_start'] = actualtime;
		  var remaining_time = parameters['end_time'] - actualtime;
		  //Browser.println("Actual " + actualtime);
		  //Browser.println("Start " + parameters['start_time']);
		  var elapsed_time = actualtime - parameters['start_time'];
		  var elapsed_part = elapsed_time/parameters['duration'];
		  var steps_to_go = Math.round(remaining_time/parameters['last_moving_duration']);
		  //Browser.println(parameters['last_moving_duration']);
		  //Browser.println('Blubb ' + steps_to_go);

		  // find the node to move
		  if (parameters.node_name === undefined && parameters.node_id === undefined) {
				Browser.println("node_name or node_id necessary");
				return
		  } else {
				if (parameters.node_id !== undefined) {
					 node = handler.findNodeByID(parameters.node_id);
				} else {
					 node = handler.findNodeByName(parameters.node_name);
					 
				}
		  }
		  if (node === undefined) {
				//Browser.println("Node not found: (ID: " + parameters.node_id + "), (Name: " + parameters.node_name + ")");
				newEvents[newEvents.length] = JSON.stringify({'time': 0, 'action': 'moveNode', 'parameters': parameters});
				return;
		  }
		  if (parameters['start_position'] === undefined) {
				parameters['start_position'] = node.position;
		  } 
		  /// find the position to move to
		  if (parameters.position === undefined && parameters.target_node === undefined) {
				Browser.println("position or target_node necessary");
				return
		  } else {
				if (parameters.target_node !== undefined) {
					 var to = handler.findNodeByName(parameters.target_node)
					 if (to === undefined) {
						  Browser.println('Target Node not found');
						  return;
					 }
					 parameters['end_position'] = to.position;
					 
				} else {
					 if (parameters['position'].split(" ").length === 3) {
						  var pos_array = parameters['position'].split(" ");
						  parameters['end_position'] = new SFVec3f(pos_array[0], pos_array[1], pos_array[2]);
					 } else {
						  Browser.println('Position in wrong format');
						  return;
					 }
				}
				// Browser.println('DiffVector ' + lengthdiffVector);
				// Browser.println('Elapsed ' + elapsed_part);
				
				//Browser.println('End ' + parameters['end_time']);
				//Browser.println('Chaser' + parameters['chaserduration']);
				//Browser.println('last_moving_duration ' + parameters['last_moving_duration']);
				
				
				if (remaining_time < parameters['chaserduration'] || remaining_time < parameters['last_moving_duration'] || parameters['end_time'] < actualtime || steps_to_go <= 0) {
					 //Browser.println("1");
					 setNewPosition(node, parameters['end_position']);
					 return;
				} else {
					 var diffVector = parameters['end_position'].subtract(parameters['start_position']);
					 var lengthdiffVector = diffVector.length();
					 if (lengthdiffVector > 0)  {
						  
						  var lengthTargetVector = lengthdiffVector / steps_to_go;
						  //Browser.println('Blobb ' + diffVector.divide(lengthdiffVector).multiply(lengthTargetVector).length());
						  setNewPosition(node, parameters['start_position'].add(diffVector.divide(lengthdiffVector).multiply(lengthTargetVector)));
					 }
				}
		  }
		  
		  //Browser.println(parameters['last_moving_duration']);
		  if (parameters['end_time'] >= actualtime) { 
				setTimeout(function(){
					 moveNode(parameters);
				},1);
				// newEvents[newEvents.length] = JSON.stringify({'time':0, 'action': 'moveNode', 'parameters': parameters});
		  } 
		  //Browser.println('move Node (finished)');

	 }



	 function moveNodeCircular(parameters) {
		  newEvents = new MFString();
		  var node;
		  var actualtime = actualFrameworkTime;
		  parameters = handler.getCompletedParameters(default_parameters, parameters);
		  if (parameters['start_time'] === undefined) {
				parameters['start_time'] = actualtime;
		  }
		  if (parameters['end_time'] === undefined) {
				parameters['end_time'] = actualtime + parameters['duration'];
		  }
		  if (parameters['end_angle'] === undefined) {
				parameters['end_angle'] = Math.PI;
		  }
		  if (parameters['angle_to_go'] === undefined) {
				parameters['angle_to_go'] = parameters['end_angle'];
		  }
		  if (parameters['actual_angle'] === undefined) {
				parameters['actual_angle'] = 0;
		  }
		  
		  //Reason for doing this this way is the X3D-peculiarity to deliver the exact same time at the start and at te end of the function, so that the duration can not be measured by instrument the beginning and the end with a classic new Date().getTime()		  
		  if (parameters['last_start'] === undefined) {
				// set start value, will be refined in the next loops
				parameters['last_moving_duration'] = 15;
		  } else {
				parameters['last_moving_duration'] = actualtime - parameters['last_start'];
		  }
		  parameters['last_start'] = actualtime;
		  var remaining_time = parameters['end_time'] - actualtime;
		  //Browser.println("Actual " + actualtime);
		  //Browser.println("Start " + parameters['start_time']);
		  var elapsed_time = actualtime - parameters['start_time'];
		  var elapsed_part = elapsed_time/parameters['duration'];
		  var steps_to_go = Math.round(remaining_time/parameters['last_moving_duration']);
		  

		  // find the node to move
		  if (parameters.node_name === undefined && parameters.node_id === undefined) {
				Browser.println("node_name or node_id necessary");
				return
		  } else {
				if (parameters.node_id !== undefined) {
					 node = handler.findNodeByID(parameters.node_id);
				} else {
					 //Browser.println('Search');
					 node = handler.findNodeByName(parameters.node_name);
					 
				}
		  }
		  if (node === undefined) {
				Browser.println("Node not found: (ID: " + parameters.node_id + "), (Name: " + parameters.node_name + ")");
				return
		  }
		  if (parameters['start_position'] === undefined) {
				parameters['start_position'] = new SFVec3f(node.position.x, node.position.y,node.position.z);
		  }
		  /// find the position to move to
		  if (parameters.position === undefined && parameters.target_node === undefined) {
				Browser.println("position or target_node necessary");
				return
		  } else {
				if (parameters.target_node !== undefined) {
					 var to = handler.findNodeByName(parameters.target_node)
					 if (to === undefined) {
						  Browser.println('Target Node not found');
						  return
					 }
					 parameters['end_position'] = to.position;
				} else {
					 if (parameters['position'].split(" ").length === 3) {
						  var pos_array = parameters['position'].split(" ");
						  parameters['end_position'] = new SFVec3f(pos_array[0], pos_array[1], pos_array[2]);
					 } else {
						  Browser.println('Position in wrong format');
					 }
				
				}
				
				var diffVector = parameters['end_position'].subtract(parameters['start_position']);
				if (diffVector.length() == 0) {
					 return;
				}
				var middlePosition = parameters['start_position'].add(diffVector.divide(2));
				if (steps_to_go <= 1) {
					 setNewPosition(node, parameters['end_position']);
					 return;
				} 
				if (parameters['angle_to_go']/steps_to_go < parameters['angle_to_go'])
				parameters['actual_angle'] = parameters['actual_angle'] + parameters['angle_to_go']/steps_to_go;
				parameters['angle_to_go'] = parameters['end_angle'] - parameters['actual_angle'];
				var zAxis = new SFVec3f(0,0,1);
				


				var normalVector = new SFVec3f(
					 (zAxis.y*diffVector.z)-(zAxis.z*diffVector.y), 
					 (zAxis.z*diffVector.x)-(zAxis.x*diffVector.z),
					 (zAxis.x*diffVector.y)-(zAxis.y*diffVector.x)
				);
				normalVector = normalVector.divide(normalVector.length());
				
				// these 4 variables are only for the purpose of making the rotationMtrix below more readable
				var ang = parameters['actual_angle'];
				var n = normalVector;
				var cosA = Math.cos(ang);
				var sinA = Math.sin(ang);

				var rotationMatrix = [
					 [n.x*n.x*(1-cosA)+cosA,n.x*n.y*(1-cosA)-n.z*sinA,((n.x*n.z*(1-cosA)) + (n.y*sinA))],
					 [n.y*n.x*(1-cosA)+n.z*sinA,n.y*n.y*(1-cosA)+cosA,((n.y*n.z*(1-cosA)) - (n.x*sinA))],
					 [n.z*n.x*(1-cosA)-n.y*sinA,n.z*n.y*(1-cosA)+n.x*sinA,((n.z*n.z*(1-cosA)) + (cosA))]
				];
			
				//         1
				//         2
            //         3
				// 1 2 3
				// 4 5 6
				// 7 8 9
				// Browser.println("StartPosition: " + parameters['start_position'].x + " " + parameters['start_position'].y + " " + parameters['start_position'].z);
				// Browser.println("EndPosition: " + parameters['end_position'].x + " " + parameters['end_position'].y + " " + parameters['end_position'].z);
				// Browser.println("MiddlePosition: " + middlePosition.x + " " + middlePosition.y + " " + middlePosition.z);
				// Browser.println("Diff: " + diffVector.x + " " + diffVector.y + " " + diffVector.z);
				// Browser.println("Normal: " + n.x + " " + n.y + " " + n.z);
				// Browser.println("Angle: " + ang);
				var normalizedPosition = parameters['start_position'].subtract(middlePosition);
				var targetPosition = new SFVec3f(
					 rotationMatrix[0][0] * normalizedPosition.x + rotationMatrix[0][1] * normalizedPosition.y + rotationMatrix[0][2] * normalizedPosition.z,
					 rotationMatrix[1][0] * normalizedPosition.x + rotationMatrix[1][1] * normalizedPosition.y + rotationMatrix[1][2] * normalizedPosition.z,
					 rotationMatrix[2][0] * normalizedPosition.x + rotationMatrix[2][1] * normalizedPosition.y + rotationMatrix[2][2] * normalizedPosition.z
					 
				).add(middlePosition);
				
				
				
				
				setNewPosition(node, targetPosition);
				
				
		  }
		  
		  //Browser.println(parameters['last_moving_duration']);
		  if (parameters['end_time'] >= actualtime) { 
				setTimeout(function(){
					 moveNode(parameters);
				},1);
				//newEvents[newEvents.length] = JSON.stringify({'time':0, 'action': 'moveNodeCircular', 'parameters': parameters});;
		  } 
		  //Browser.println('move Node (finished)');

	 }




	 handler.initialize();
	 return handler;
})();
