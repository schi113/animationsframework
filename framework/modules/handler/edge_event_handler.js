importScripts('handler/handler_prototype.js');

var EdgeEventHandler = (function(){
	 var actions = {
		  'addEdge': addEdge, 
		  'removeEdge': removeEdge
	 };
	 var default_parameters = {
		  'edge_type': 'Arrow', 
		  'create_routes' : true
	 };


	 var handler = new Handler(actions);

	 function linkNodes(from, to, arrow){
		  var source = handler.findNodeByName(from);
		  var target = handler.findNodeByName(to);
		  
		  if (source) {
				Browser.currentScene.addRoute(source, 'position', arrow, 'source');
		  } else {
				Browser.println("Source nicht gefunden");
		  }
		  if (target) {
				Browser.currentScene.addRoute(target, 'position', arrow, 'target');
		  } else {
				Browser.println("Target nicht gefunden");
		  }

	 }

	 function addEdge(parameters) {
		  parameters = handler.getCompletedParameters(default_parameters, parameters);
		  var from = parameters.from;
		  var to = parameters.to;
		  if (parameters.from == parameters.to) {
				// nichts zu tun
				return 
		  }
		  var arrow = new SFNode(parameters['edge_type']);
		  var source = handler.findNodeByName(from);
		  var target = handler.findNodeByName(to);
		  if (source && target) {
				var uid = handler.getUID();
				arrow.source = source.position;
				arrow.sourcename = from;
				arrow.target = target.position;
				arrow.targetname = to;
				arrow.name = parameters.edge_name;
				if (parameters.color !== undefined) {
					 if (parameters.color.split(' ').length === 3) {
						  var color = new SFColor(parameters.color.split(' ')[0], parameters.color.split(' ')[1], parameters.color.split(' ')[2]);
						  arrow.linecolor = color;
						  arrow.arrowheadcolor = color;
					 } else {
						  Browser.println("Farbe nicht parsebar: " + parameters.color);
					 }
				}
				if (parameters.linecolor !== undefined) {
					 if (parameters.linecolor.split(' ').length === 3) {
						  var color = new SFColor(parameters.linecolor.split(' ')[0], parameters.linecolor.split(' ')[1], parameters.linecolor.split(' ')[2]);
						  arrow.linecolor = parameters.linecolor;
					 } else {
						  Browser.println("Farbe nicht parsebar: " + parameters.linecolor);
					 }
				}
				if (parameters.arrowheadcolor) {
					 if (parameters.arrowheadcolor.split(' ').length === 3) {
						  var color = new SFColor(parameters.arrowheadcolor.split(' ')[0], parameters.arrowheadcolor.split(' ')[1], parameters.arrowheadcolor.split(' ')[2]);
						  arrow.arrowheadcolor = parameters.arrowheadcolor;
					 } else {
						  Browser.println("Farbe nicht parsebar: " + parameters.arrowheadcolor);
					 }
				}
				handler.getEdgesGroup().addChild(arrow);
				if (parameters['create_routes'] == true) {
					 linkNodes(from, to, arrow);
				}
		  } else {
				Browser.println("Quelle oder Ziel nicht gefunden: von: " + parameters.from + " nach: " + parameters.to);
				// setTimeout(function(){
				// 	 addEdge(parameters);
				// },1);
		  }
	 }

	 function removeEdge(parameters) {
		  var edge_to_remove = handler.findEdgeByName(parameters.edge_name);
		  if (edge_to_remove) {
				handler.getEdgesGroup().removeChild(edge_to_remove);
		  } else {
				Browser.println('Kante nicht gefunden');
		  }
	 }


	 handler.initialize();
	 return handler;
})();
