importScripts('handler/handler_prototype.js');

var NodePropertyChangeHandler = (function(){
	 var actions = {
		  'changeSize': changeSize,
		  'changeColor': changeColor,
		  'changeEmissiveColor': changeEmissiveColor,
		  'changeTransparency': changeTransparency,
		  
	 };
	 var handler = new Handler(actions);

	 // repetition 0 -> value goes to target_value and does not go back
	 // repetition 1 -> value goes to target_value and  goes back
	 // repetition > 1 -> value goes to target_value and goes back n times
	 // repetition -1 -> value goes to target_value and goes back infinite times
	 var size_default_parameters = {
		  'node_name': '', 
		  'duration': 10000,
		  'target_value': "4.0 4.0 4.0",
		  'repetition': 0
	 };

	 var color_default_parameters = {
		  'node_name': '', 
		  'duration': 10000,
		  'target_value': "1.0 1.0 1.0",
		  'repetition': 0
	 };

	 var transparency_default_parameters = {
		  'node_name': '', 
		  'duration': 10000,
		  'target_value': "1.0",
		  'repetition': 0
	 };
	 
	 function changeTransparency(parameters) {
		  parameters = handler.getCompletedParameters(size_default_parameters, parameters);
		  changeProperty(parameters, 'size');
	 }

	 function changeSize(parameters) {
		  parameters = handler.getCompletedParameters(size_default_parameters, parameters);
		  changeProperty(parameters, 'size');
	 }

	 function changeColor(parameters) {
		  parameters = handler.getCompletedParameters(size_default_parameters, parameters);
		  changeProperty(parameters, 'color');
	 }

	 function changeEmissiveColor(parameters) {
		  parameters = handler.getCompletedParameters(size_default_parameters, parameters);
		  changeProperty(parameters, 'emissivecolor');
	 }


	 function changeProperty(parameters, attribute) {
		  //Browser.println("Change Property: " + attribute);
		  parameters = handler.getCompletedParameters(size_default_parameters, parameters);
		  var eventtime = handler.getActualFrameworkTime();
		  var node = handler.findNodeByName(parameters.node_name);
		  var old_value,new_value,key_value,interpolator;
		  if (node) {
				switch(attribute) {
				case 'size':
					 interpolator = new SFNode('PositionInterpolator');
					 if (parameters['target_value'].split(' ').length === 3) {
	 	  				  new_value = new SFVec3f(parameters['target_value'].split(' ')[0], parameters['target_value'].split(' ')[1], parameters['target_value'].split(' ')[2]);
					 } else {
						  new_value = new SFVec3f(parameters['target_value'], parameters['target_value'], parameters['target_value']);
					 }
					 if (parameters['old_value'] !== undefined) {
						  old_value = new SFVec3f(parameters['old_value']['x'], parameters['old_value']['y'], parameters['old_value']['z']);
					 } else {
						  old_value = node.size;
					 }
					 key_value = new MFVec3f();
					 break;
				case 'color':
					 interpolator = new SFNode('ColorInterpolator');
					 if (parameters['target_value'].split(' ').length === 3) {
	 	  				  new_value = new SFColor(parameters['target_value'].split(' ')[0], parameters['target_value'].split(' ')[1], parameters['target_value'].split(' ')[2]);
					 } else {
						  new_value = new SFColor(parameters['target_value'], parameters['target_value'], parameters['target_value']);
					 }
					 if (parameters['old_value'] !== undefined) {
						  old_value = new SFColor(parameters['old_value']['r'],parameters['old_value']['g'],parameters['old_value']['b']);
					 } else {
						  old_value = node.color;
					 }
					 key_value = new MFColor();
					 break;
				case 'emissivecolor':
					 interpolator = new SFNode('ColorInterpolator');
					 if (parameters['target_value'].split(' ').length === 3) {
	 	  				  new_value = new SFColor(parameters['target_value'].split(' ')[0], parameters['target_value'].split(' ')[1], parameters['target_value'].split(' ')[2]);
					 } else {
						  new_value = new SFColor(parameters['target_value'], parameters['target_value'], parameters['target_value']);
					 }
					 if (parameters['old_value'] !== undefined) {
						  old_value = new SFColor(parameters['old_value']['r'],parameters['old_value']['g'],parameters['old_value']['b']);
					 } else {
						  old_value = node.color;
					 }
					 key_value = new MFColor();
					 break;
				default:
					 Browser.println('unknown attribute to change');
					 return;
				}
				var time_sensor = new SFNode('TimeSensor');
				var key = new MFFloat();
				if (parameters['repetition'] == 0) {
					 key_value[0] = old_value;
					 key_value[1] = new_value;
					 key[0] = 0;
					 key[1] = 1;
				} else {
					 key_value[0] = old_value;
					 key_value[1] = new_value;
					 key_value[2] = old_value;
					 key[0] = 0;
					 key[1] = 0.5;
					 key[2] = 1;
				}
				interpolator.keyValue = key_value;
				interpolator.key = key;
<<<<<<< HEAD
				time_sensor.cycleInterval = parameters['duration']/1000;
				if (parameters['repetition'] == -1) {
					 time_sensor.loop = true;
				} else {
					 time_sensor.loop = false;
				}
=======
				time_sensor.cycleInterval = parameters['duration']/1000.0;
				time_sensor.loop = false;
>>>>>>> 9a58b7de66b1747c5768afdd490066d512d30424
				handler.getNodesGroup().addChild(interpolator);
				handler.getNodesGroup().addChild(time_sensor);
				Browser.currentScene.addRoute(time_sensor, "fraction_changed", interpolator, 'set_fraction');
				Browser.currentScene.addRoute(interpolator, "value_changed", node, attribute);
				time_sensor.startTime = new Date().getTime()/1000;
				//Browser.println("Change Property " + attribute + " done");
				


		  } else {
				Browser.println('Node not found');
		  }
		  if (parameters['repetition'] > 1) {
				newEvents = new MFString();
				var action;
				switch(attribute) {
				case 'size':
					 action = 'changeSize';
					 break;
				case 'color':
					 action = 'changeColor';
					 break;
				case 'emissivecolor':
					 action = 'changeEmissiveColor';
					 break;
				case 'transparency':
					 action = 'changeTransparency';
					 break;
				}
				var repetition_times = parameters['repetition'];
				parameters['repetition'] = 1;
				parameters['old_value'] = old_value;
				for (var i = 1; i < repetition_times; i++) {
					 Browser.println(JSON.stringify({"time":eventtime,"action":action,"parameters": parameters}));
					 eventtime += parameters['duration'];
					 newEvents[newEvents.length] = JSON.stringify({"time":eventtime,"action":action,"parameters": parameters});
				}
		  }

	 }



	 handler.initialize();
	 return handler;
})();
