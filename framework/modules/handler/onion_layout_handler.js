importScripts('handler/handler_prototype.js');

var OnionLayoutHandler = (function(){
	 var actions = {
		  'onionLayout': onionLayout
	 };	 
	 var handler = new Handler(actions);

	 var layout_name = "OnionLayout";

	 var default_parameters = {
		  cooling_factor: 0.98, 
		  dimension_multiplier: 30, 
		  optimal_distance_factor: 0.1, 
		  optimal_distance_center_factor: 1.5,
		  last_node_count: 0,
		  'default' : false,
		  temperature: 2
		  
	 };


	 function setNodesToLayout(parameters) {
		  nodes_to_layout = [];
		  // Browser.println("Anzahl Knoten gesamt: " + nodes_group.children.length);
		  if (parameters['default'] == true) {
				// Browser.println('ist Defaultlayout');
				for (var i = 0; i < handler.getNodesGroup().children.length; i++) {
					 // Browser.println(handler.getNodesGroup().children[i]['usedefaultlayout']);
		   		 if (handler.getNodesGroup().children[i]['usedefaultlayout'] === true) {
						  nodes_to_layout.push(handler.getNodesGroup().children[i]);
		   		 }
		   	} 
		  } else {
				for (var i = 0; i < handler.getNodesGroup().children.length; i++) {
		  			 if (handler.getNodesGroup().children[i].layout == parameters['layout_name']) {
		  				  nodes_to_layout.push(handler.getNodesGroup().children[i]);
		  			 }
		  		} 
		  }
		  return nodes_to_layout;
	 }


	 function setNodePosition(node, targetposition) {
		  newEvents[newEvents.length] = JSON.stringify({'time': 0, 'action': 'moveNode', 'parameters':{'node_name': node.name, 'position': targetposition.x + ' ' + targetposition.y + ' ' + targetposition.z, 'duration': 2000}});
	 }
	 
	 function optimalDistance(area, optimal_distance_factor, nodes_to_layout_length){
		  return optimal_distance_factor * (Math.pow(area/nodes_to_layout_length,1/4));
	 }

	 function optimalDistanceCenter(area, attraction, optimal_distance_center_factor, node_count){
	  	  return optimal_distance_center_factor * (1-attraction) * (Math.pow(area/node_count,1/4));
	 }

	 function attractiveForcesCenter(attraction, distance, od) {
	  	  return (Math.pow(distance,8)/od) * (attraction);
	  }

	 function repulsiveForcesCenter(attraction, distance, od) {
	  	  return (Math.pow(od,8)/distance) * (attraction);
	 }

	 function coolTemperature(t, cooling_factor) {
		  return Math.pow(t, cooling_factor);
	 }
	 
	 function attractiveForces(distance, od) {
		  return (distance*distance*distance*distance)/od;
	 }
	 
	 function repulsiveForces(distance, od) {
		  return ((od*od*od*od)/distance);
	 }



	 // als erstes war angedacht, die Knoten bei jedem Durchlauf neu zu  positionieren, nun wird versucht, die Position zu berechnen und die Knoten danach umzupositionieren 

	 function getNodeAndEdgeInformations(nodes_to_layout) {
		  var node_array  = [];
		  var edge_array  = [];
		  for (var i = 0; i < nodes_to_layout.length ; i++){
				var node = nodes_to_layout[i];
				var node_object = {'targetposition': node['position'], 'displacement': new SFVec3f(0,0,0), 'name': node['name'], 'centerattraction': node['centerattraction']};
				node_array.push(node_object);
		  }
		  for (var i = 0; i < handler.getEdgesGroup().children.length; i++) {
				var edge_object = {'from': handler.getEdgesGroup().children[i]['sourcename'], 'to': handler.getEdgesGroup().children[i]['targetname']}
				edge_array[i] = edge_object;
		  }
		  return [node_array, edge_array];
	 }

	 function findNodeInformationByName(name, nodes_information) {
		  for (var i = 0; i < nodes_information.length; i++) {
				if (nodes_information[i]['name'] === name) {
					 return nodes_information[i];
				}
		  }
		  return undefined
	 }

	 function setPositions(nodes_information, nodes_to_layout) {
		  Browser.println('Length: ' + nodes_to_layout.length);
		  
		  for (var i = 0; i < nodes_to_layout.length; i++) {
				var node = nodes_to_layout[i];
				if (node !== undefined) {
					 for (var j = 0; j < nodes_information.length;j++) {
						  if (nodes_information[j]['name'] == node['name']) {
								setNodePosition(node, nodes_information[j]['targetposition']);
						  }
					 }
				} else {
					 Browser.println('OOh');
				}
		  }
	 }




	 // this ist the bottleneck of the whole layout-algorithm because of the m^2 calculations
	 function calculateRepulsiveForces(nodes_information, od) {
		  var node_count = nodes_information.length;
		  for (var i = 0; i < node_count ; i++) {
				//.disp = displacement
				//.pos = position
				var node1 = nodes_information[i];
				node1['displacement'] = new SFVec3f(0,0,0);
				for (var j = 0; j < node_count ; j++) {
					 if (j !== i) {
						  var node2 = nodes_information[j];
						  // if (node1['targetposition'] == undefined || node2['targetposition'] == undefined) { 
						  //  	 Browser.println('########################');
						  // 	 for (var k = 0; k < node_count; k++) {
						  // 		  Browser.println('########################');
						  // 		  Browser.println(nodes_information[k]);
						  // 		  Browser.println(nodes_information[k]['target_position']);
						  // 		  Browser.println(nodes_information[k]['name']);
						  // 	 }
						  //  }
						  var diffVector = node1['targetposition'].subtract(node2['targetposition']);
						  var lengthDiffVector = diffVector.length();
						  node1['displacement'] = node1['displacement'].add(diffVector.divide(lengthDiffVector).multiply(repulsiveForces(lengthDiffVector, od)));
					 }
				}
		  }
	 }


	 function onionLayout(parameters){
		  var nodes_to_layout = setNodesToLayout(parameters);
		  parameters = handler.getCompletedParameters(default_parameters, parameters);
		  if (parameters['nodes_information'] !== undefined && parameters['edges_information'] !== undefined && parameters['last_node_count'] === nodes_to_layout.length) 
		  {
				var nodes_information = parameters['nodes_information'];
				var edges_information = parameters['edges_information'];
		  } else {
				Browser.println("Layout");
				var information_object = getNodeAndEdgeInformations(nodes_to_layout);
				var nodes_information = information_object[0];
				var edges_information = information_object[1];
				parameters['nodes_information'] = nodes_information;
				parameters['edges_information'] = edges_information;
				
		  }
		  var node_count = nodes_information.length;
		  
		  if (parameters['last_node_count'] !== node_count) {
				Browser.println("Heating Up");
		   	parameters['temperature'] = 2;
				parameters['positions_set'] = false;
		  }
		  
		  var temperature = parameters['temperature'];		  
		  if (temperature > 1.10) {
				var cooling_factor = parameters['cooling_factor'];
				var dimension_multiplier = parameters['dimension_multiplier'];
				var optimal_distance_factor = parameters['optimal_distance_factor'];
				var optimal_distance_center_factor = parameters['optimal_distance_center_factor'];
				var width = (node_count * dimension_multiplier);
				var length = (node_count * dimension_multiplier);
				var height = (node_count * dimension_multiplier);
				var area = width * length * height;
				var od = optimalDistance(area, optimal_distance_factor, node_count);
				//Browser.println('LAYOUT' + parameters['temperature']);
				//repulsive forces

				calculateRepulsiveForces(nodes_information, od);

				// attractive forces between 2 nodes
				//Browser.println(edges_information.length);
				for (var i = 0; i < edges_information.length; i++) {
					 var from = findNodeInformationByName(edges_information[i]['from'], nodes_information);
					 var to = findNodeInformationByName(edges_information[i]['to'], nodes_information);
					 if (from && to) {
						  var diffVector = from['targetposition'].subtract(to['targetposition']);
						  var lengthDiffVector = diffVector.length();
						  from['displacement'] = from['displacement'].subtract((diffVector.divide(lengthDiffVector).multiply(attractiveForces(lengthDiffVector, od))));
						  to['displacement'] = to['displacement'].add((diffVector.divide(lengthDiffVector).multiply(attractiveForces(lengthDiffVector, od))));
					 }
				}
				
				for (var i = 0; i < node_count ; i++) {
					 // adding forces from center
					 var node = nodes_information[i];
					 if (node['centerattraction'] != -1.0) {
						  var centerPosition = new SFVec3f(0,0,0);
						  var diffVectorCenter = node['targetposition'].subtract(centerPosition);
						  var lengthDiffVectorCenter = diffVectorCenter.length();
						  node['displacement'] = node['displacement'].subtract((diffVectorCenter.divide(lengthDiffVectorCenter).multiply(attractiveForcesCenter(node['centerattraction'], lengthDiffVectorCenter, optimalDistanceCenter(area, node['centerattraction'], optimal_distance_center_factor, node_count)))));
						  node['displacement'] = node['displacement'].add((diffVectorCenter.divide(lengthDiffVectorCenter).multiply(repulsiveForcesCenter(node['centerattraction'], lengthDiffVectorCenter, optimalDistanceCenter(area, node['centerattraction'], optimal_distance_center_factor, node_count)))));
						  
					 } 
				}

				
				// limit maximum displacement to temperature t not done, only setting the calculated forces
				for (var i = 0; i < node_count ; i++) {
					 var node = nodes_information[i];
					 var lengthDisplacementVector = node['displacement'].length();
					 //setNodePosition(node, node.targetposition.add((node.displacement.divide(lengthDisplacementVector).multiply(Math.min(lengthDisplacementVector, temperature)))));
					 // Browser.println(lengthDisplacementVector);
					 // Browser.println(node['displacement'].x);
					 // Browser.println(node['displacement'].y);
					 // Browser.println(node['displacement'].z);
					 node['targetposition'] = node['targetposition'].add((node['displacement'].divide(lengthDisplacementVector).multiply(Math.min(lengthDisplacementVector, temperature))));
					 
					 //node.targetposition.x = Math.min((width/2), Math.max(-width/2, node.targetposition.x));
					 //node.targetposition.y = Math.min((length/2), Math.max(-length/2, node.targetposition.y));
					 //node.targetposition.z = Math.min((height/2), Math.max(-height/2, node.targetposition.y));
				}
				
				parameters['last_node_count'] = node_count;
				
				parameters['temperature'] = coolTemperature(temperature, cooling_factor);
				

				//newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"onionLayout","parameters":parameters});

				setTimeout(function() {
				 	 onionLayout(parameters)
				}, 1);
					 
		  } else {
				// set positions one time
		  
						  

				if (parameters['positions_set'] == false || parameters['positions_set'] == undefined) {
					 Browser.println("Set Positions");
					 setPositions(nodes_information,nodes_to_layout);
					 parameters['positions_set'] = true;
				}
				
				newEvents[newEvents.length] = JSON.stringify({"time":0,"action":"onionLayout","parameters":parameters});
				// setTimeout(function() {
				// 	 onionLayout(parameters)
				// }, 1);
				
		  }
		  
	 }

	 handler.initialize();
	 return handler;
})();
